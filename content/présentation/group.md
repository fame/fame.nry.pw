---

## title: "le groupe" date: 2018-11-15T14:12:19+01:00 draft: false

La FAME est également un groupe de musiciens, né en 2016 durant le mouvement des Nuits Debouts, dans l'idée d'exploiter la musique comme force de soutien, voire de proposition, dans un cadre activiste et/ou militant. Nous combattons leur argent avec nos cuivres. Son effectif total est pour l'instant estimé à 25 actifs, pour une moyenne de huit personnes par sortie. L'adhésion au groupe est entièrement ouverte. Aucune expertise musicale n'est évidemment requise.

## **Répétitions tous les mardi dès 19h au [Chat Perché](https://www.chatperche.org/)¹**

¹Dont l'équipe nous prête plus qu'aimablement les locaux (auto-chauffés par chaleur humaine). Qu'elle en soit ici grâcement remerciée.

###### 