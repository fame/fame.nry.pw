---
title: "Chant des marais"
date: 2019-01-16T15:54:07+01:00
draft: false
---

*I*<br/>
Loin vers l'infini s'étendent<br/>
De grands prés marécageux<br/>
Et là-bas nul oiseau ne chante<br/>
Sur les arbres secs et creux<br/>
<br/>
*Refrain*<br/>
Ô terre de détresse<br/>
Où nous devons sans cesse<br/>
Piocher, piocher.<br/>
<br/>
II*<br/*>
Dans ce camp morne et sauvage<br/>
Entouré de murs de fer<br/>
Il nous semble vivre en cage<br/>
Au milieu d'un grand désert.<br/>
<br/>
*III*<br/>
Bruit des pas et bruit des armes<br/>
Sentinelles jours et nuits<br/>
Et du sang, et des cris, des larmes<br/>
La mort pour celui qui fuit.<br/>
<br/>
IV**<br/>
Mais un jour dans notre vie<br/>
Le printemps refleurira.<br/>
Liberté, liberté chérie<br/>
Je dirai : « Tu es à moi. »<br/>
<br/>
*Dernier refrain*<br/>
Ô terre enfin libre<br/>
Où nous pourrons revivre,<br/>
Aimer, aimer.<br/>
<br/>
*Dernier refrain*<br/>
Ô terre d’allégresse<br/>
Où nous pourrons sans cesse<br/>
Aimer, aimer<br/>
 