---
title: "Avançons à l'unisson"
date: 2018-11-14T20:54:07+01:00
draft: false
---

Inébranlables,<br \>
Infatigables<br \>
Avançons, avançons, avançons<br \>
Du monde en descente<br \>
Remontons la pente<br \>
Avançons à l'unisson<br \>
<br \>
 Sur nous ils spéculent<br \>
Ils rêvent qu'on recule<br \>
Avançons, avançons, avançons<br \>
Ils pensent qu'à leur gueule<br \>
Ils se croient tous seuls<br \>
Avançons à l'unisson<br \>
<br \>
 Tout le mond’ se couche<br \>
Dès qu'ils ouvr' la bouche<br \>
Avançons, avançons, avançons<br \>
La justice détale<br \>
Et l'Etat s'étale<br \>
Avançons à l'unisson<br \>
<br \>
 Veulent nous encager<br \>
Pour tout diriger<br \>
Avançons, avançons, avançons<br \>
Ils dirigent c'est sûr<br \>
Tout droit dans le mur <br \>
Avançons à l'unisson<br \>
<br \>
 Pour pas s'écraser <br \>
On va tout freiner<br \>
Avançons, avançons, avançons<br \>
Et en s'écoutant<br \>
Reprendre le temps<br \>
Avançons à l'unisson<br \>
<br \>
 Nous on ne sait pas<br \>
Vraiment où on va<br \>
Avançons, avançons, avançons<br \>
Mais main dans la main<br \>
C'est un autr' chemin<br \>
Avançons à l'unisson<br \>
<br \>
 Mais main dans la main<br \>
C'est un notr' chemin <br \>
Avançons à l'unisson (ralentissant)<br \>
