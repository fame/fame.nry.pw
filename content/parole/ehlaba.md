---
title: "Hé là-bas (Ôde aux casseur·euses)"
date: 2020-01-21T20:54:07+01:00
draft: false
---

Paroles des Canulars (Lyon), 2019 <br \>

<br \>
En mille sept cent quatre-vingt neuf, <br \>
Des gueux ont attaqué les keufs, <br \>
À coups de fourches et de bâtons, <br \>
Ils ont libéré la prison, <br \>
<br \>
Et tous les 14 juillet, <br \>
Quand t'applaudis le défilé, <br \>
T'oublies de dire, j'me demande  pourquoi, <br \>
Qu'ils ont coupé la tête au roi <br \>
<br \>
Refrain : <br \>
Non non non x2, <br \>
C’est pas bien d’ casser x2 <br \>
Sauf quand on x2, <br \>
Quand on a gagné x2 <br \>
<br \>
Pendant la guerre les maquisards, <br \>
Faisaient sauter les trains les gares, <br \>
Aujourd'hui tu leur rends hommage, <br \>
Toujours au passé c'est dommage, <br \>
<br \>
Et quand aux monuments aux morts, <br \>
Tu les véneres tu les honores, <br \>
T'oublies de dire que les fascistes, <br \>
Les traitaient de terroristes, <br \>
<br \>
Refrain <br \>
<br \>
Mille neuf cent trois, les meufs anglaises, <br \>
Avaient osé c'est balèze, <br \>
Casser les vitres des entreprises, <br \>
Et foutre le feu aux églises, <br \>
<br \>
Et quand pour les présidentielles, <br \>
Tu loues l'suffrage universel, <br \>
T'oublies de dire c'est pas normal, <br \>
Qu'c'est grâce à ça si c'est légal, <br \>
<br \>
Refrain <br \>
<br \>
Quand dans les manifestations, <br \>
On dépave les illusions, <br \>
Et qu'on balance des utopies, <br \>
À la gueule de la bourgeoisie, <br \>
<br \>
En été quand tu vas bronzer, <br \>
Quand tes médocs sont remboursés, <br \>
T'oublies que grâce à cette violence, <br \>
T'as la sécu et tes vacances, <br \>
<br \>
Refrain <br \>
<br \>
Non non non <br \>
C'est pas bien d'casser <br \>
Et on va <br \>
Et on va gagner <br \>




