---
title: "Slogans RESF2017"
date: 2018-12-14T16:49:07+01:00
draft: false
---

Proposition de slogans de soutien aux réfugiéEs <br \>


Non, non, non, à la loi Macron <br \>
liberté de circulation, et d’installation <br \>

Un toit, une école, des papiers pour tous <br \>

So, so, so, solidarité <br \>
Avec les réfugiéEs, du monde entier ! <br \>

Un logement pour tous ! La solution : réquisition <br \>

De l’air, de l’air : ouvrez les frontières ! <br \>

ABROGATION de toutes les lois racistes <br \>
Solidarité internationaliste <br \>

C'est pas les sans papiers qu'il faut virer, <br \>
c'est le racisme et la précarité <br \>

Des familles à la rue, on n’en veut plus
Des papiers pour tous, des logements pour tous ! 
