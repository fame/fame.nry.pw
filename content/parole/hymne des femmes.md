---
title: "Hymne des femmes"
date: 2019-01-14T20:54:07+01:00
draft: false
---

Nous, qui sommes sans passé, les femmes,<br/>
Nous qui n'avons pas d'histoire,<br/>
Depuis la nuit des temps, les femmes,<br/>
Nous sommes le continent noir.<br/>
<br/>
*Refrain :*<br/>
Debout femmes esclaves<br/>
Et brisons nos entraves<br/>
Debout ! debout !<br/>
<br/>
Asservies, humiliées, les femmes,<br/>
Achetées, vendues, violées,<br/>
Dans toutes les maisons, les femmes,<br/>
Hors du monde reléguées.<br/>
<br/>
*Refrain*<br/>
<br/>
Seule dans notre malheur, les femmes,<br/>
L'une de l'autre ignorée,<br/>
Ils nous ont divisées, les femmes,<br/>
Et de nos sœurs séparées.<br/>
<br/>
*Refrain*<br/>
<br/>
Reconnaissons-nous, les femmes,<br/>
Parlons-nous, regardons-nous,<br/>
Ensemble on nous opprime, les femmes,<br/>
Ensemble révoltons-nous.<br/>
<br/>
*Refrain*<br/>
<br/>
Le temps de la colère, les femmes,<br/>
Notre temps est arrivé,<br/>
Connaissons notre force, les femmes,<br/>
Découvrons-nous des milliers.<br/>
<br/>
*Dernier refrain :*<br/>
Levons-nous femmes esclaves<br/>
Et jouissons sans entrave<br/>
Debout, debout, debout !<br/>
 