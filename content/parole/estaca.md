---
title: "Estaca"
date: 2018-11-14T20:54:07+01:00
draft: false
---

 Du temps où je n'étais qu'un gosse <br \>
Mon grand-père me disait souvent <br \>
Assis à l'ombre de son porche <br \>
En regardant passer le vent <br \>
Petit vois-tu ce pieu de bois <br \>
Auquel nous sommes tous enchaînés <br \>
Tant qu'il sera planté comme ça <br \>
Nous n'aurons pas la liberté <br \>
<br \>
Mais si nous tirons tous, il tombera <br \>
Ca ne peut pas durer comme ça <br \>
Il faut qu'il tombe, tombe, tombe <br \>
Vois-tu comme il penche déjà <br \>
Si je tire fort il doit bouger <br \>
Et si tu tires à mes côtés <br \>
C'est sûr qu'il tombe, tombe, tombe <br \>
Et nous aurons la liberté <br \>
<br \>
Petit ça fait déjà longtemps <br \>
Que je m'y écorche les mains <br \>
Et je me dis de temps en temps <br \>
Que je me suis battu pour rien <br \>
Il est toujours si grand si lourd <br \>
La force vient à me manquer <br \>
Je me demande si un jour <br \>
Nous aurons bien la liberté <br \>
<br \>
Mais si nous... <br \>
<br \>
Puis mon grand-père s'en est allé <br \>
Un vent mauvais l'a emporté <br \>
Et je reste seul sous le porche <br \>
En regardant jouer d'autres gosses <br \>
Dansant autour du vieux pieu noir <br \>
Où tant de mains se sont usées <br \>
Je chante des chansons d'espoir <br \>
Qui parlent de la liberté <br \>
<br \>
Mais si nous...<br \>

