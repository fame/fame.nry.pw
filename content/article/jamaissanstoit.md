---
title: Jamais sans toit
date: 2018-11-26T18:30:00+01:00
draft: false
tags:
  - news
---
La FAME était présente (en petit nombre, certes), aux côtés de la chorale des Canulars, devant l'école Berthelot, ce lundi 26 novembre 2018, en soutien aux enfants et leurs familles sans logement.

***ps : si toi aussi, tu veux jouer pour les enfants, rejoins la FAME! Elle n'attends que toi !***