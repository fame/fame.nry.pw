---
title: "les bonnes adresses"
date: 2018-11-14T15:25:02+01:00
draft: false
tags: [structure, activisme, titubanda]
---

- Tu joues vaguement d'un instrument de musique ? Tu aimes déclamer des slogans dans un mégaphone usé ? Tu as envie de rejoindre la FAME ?

Écris à <a href="mailto:sympa@lists.riseup.net?Subject=subscribe%20fame-lyon" target="_top">sympa@lists.riseup.net</a> avec pour objet **subscribe fame-lyon**.

*Tu seras alors ajouté à la liste mail qui te permettra de répondre aux propositions de manifs/actions militantes et d'en proposer toi-même.
Les partitions et paroles sont accessibles en ligne, et la FAME répète une fois par mois. Ou du moins elle essaye.*

- Tu souhaites demander aux musiciens de la FAME de soutenir un événement militant ?

 Écris à <a href="mailto:famelyon@disroot.org" target="_top">famelyon@disroot.org</a>.

*Par le truchement d'une liste mail et d'un sondage en ligne haute-technologie, ils se concerteront pour te confirmer ou non leur présence dans les jours qui suivent.*
